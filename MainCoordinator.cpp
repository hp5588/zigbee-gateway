#include <iostream>
#include <src/CoordinatorApp.h>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <src/gui/DevicePageController.h>
#include <variable.h>

using namespace std;


int arc ;
char **arv;


int main(int argc, char *argv[]) {

    arc = argc;
    arv= argv;

    auto console = spdlog::stdout_color_mt("main");

    spdlog::set_level(spdlog::level::debug);


    console->info("App Starting");

    CoordinatorApp *app = new CoordinatorApp;
    app->run();



    console->info("App Stopped");

    return 0;
}
