cmake_minimum_required(VERSION 3.5)

set(CMAKE_C_COMPILER arm-linux-gnueabihf-gcc)
set(CMAKE_CXX_COMPILER arm-linux-gnueabihf-g++)
set(CMAKE_PREFIX_PATH /home/brian/raspi/qt5pi/lib/cmake)
#set(CMAKE_SYSROOT  /home/brian/build/Qt/sysroot)
set(CMAKE_SYSROOT  /mnt/rasp-pi-rootfs)

project(zigbee_coordinator)

include(ExternalProject)

set(CMAKE_CXX_STANDARD 11)
# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_BUILD_TYPE Debug)


set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(EXTERNAL_INSTALL_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/install)
set(SRC_LOCATION ${CMAKE_CURRENT_BINARY_DIR}/3rdparty)
set(SPDLOG_SRC_LOCATION ${SRC_LOCATION}/spdlog)


#set(Qt5_DIR /home/brian/raspi/qt5pi/lib/cmake/Qt5)

#set(CMAKE_PREFIX_PATH /usr/local/qt5pi)
#set(CMAKE_PREFIX_PATH /usr/local/qt5pi/lib/cmake)
#set(Qt5_DIR /usr/local/qt5pi)
#set(QT5_DIR /usr/local/qt5pi/lib/cmake)


link_directories(
        /mnt/rasp-pi-rootfs/lib/arm-linux-gnueabihf
#        /mnt/rasp-pi-rootfs/usr/lib/arm-linux-gnueabihf
        /mnt/rasp-pi-rootfs/usr/lib
        )
#QT setup
#FILE(GLOB QTROOTS /home/brian/raspi/qt5pi/bin)
#FIND_PROGRAM(QT_QMAKE_EXECUTABLE NAMES qmake qmake5 qmake-qt5 qmake-mac PATHS ${QTROOTS})



#find_package(Qt5Widgets)

#foreach(QT_LIBRARIES_REQUIRED ${QT_LIBRARIES_REQUIRED})
#    find_package( ${QT_LIBRARIES_REQUIRED} REQUIRED )
#endforeach()

#add_custom_target(qtMakefile COMMAND make -C ${QT_BIN})

#include_directories(${QT_BIN})





ExternalProject_Add(
        spdlog
        GIT_REPOSITORY "https://github.com/gabime/spdlog.git"
#        GIT_TAG "master"
        SOURCE_DIR "${SPDLOG_SRC_LOCATION}"
        CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)


#QT
find_package(Qt5 COMPONENTS Quick Core Qml)
qt5_add_resources(RCC_SOURCES src/gui/qml.qrc)


# Locate libraries and headers
#find_package(WiringPi REQUIRED)
find_package (Threads)
# Include headers
find_path(WIRINGPI_INCLUDE_DIRS NAMES wiringPi.h)
include_directories(src/define)


#find library
find_library(WIRINGPI_LIBRARIES NAMES wiringPi)
find_library( rt_lib rt)
include_directories(${WIRINGPI_INCLUDE_DIRS})

#spdlogger
include_directories(${EXTERNAL_INSTALL_LOCATION}/include)
link_directories(${EXTERNAL_INSTALL_LOCATION}/lib)

include_directories(src/gui)


# Link against libraries
#file(GLOB SOURCE_FILES src/*.cpp src/*.h)
set(SOURCE_FILES
        src/define/CC2530.h
        src/ZigbeeZnp.cpp
        src/ZigbeeZnp.h
        src/define/CC2530.h
        src/devices/DeviceBase.h
        src/devices/Coordinator.cpp
        src/devices/Coordinator.h
        src/devices/EndDevice.cpp
        src/devices/EndDevice.h
        src/devices/Router.cpp
        src/devices/Router.h
        src/devices/DeviceBase.cpp
        src/tools/Command.cpp
        src/tools/Command.h
        src/tools/Response.cpp
        src/tools/Response.h
        src/define/AppParms.h
        src/CoordinatorApp.cpp
        src/CoordinatorApp.h
        src/AppBase.h
        src/AppBase.cpp
        src/RouterApp.h
        src/RouterApp.cpp
#        QT source


        src/devices/DeviceModel.cpp src/devices/DeviceModel.h variable.h)

set(COOR_SOURCE_FILES ${SOURCE_FILES}  MainCoordinator.cpp
#        src/gui/DeviceObject.h
        src/gui/DeviceObject.cpp
#        src/gui/DevicePageController.h
        src/gui/DevicePageController.cpp
        )
add_executable(coordinator  ${COOR_SOURCE_FILES} ${RCC_SOURCES})
add_dependencies(coordinator spdlog)
target_link_libraries(coordinator ${WIRINGPI_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT}  rt crypt Qt5::Qml Qt5::Quick)


set(ROUTER_SOURCE_FILES ${SOURCE_FILES} MainRouter.cpp)
add_executable(router ${ROUTER_SOURCE_FILES})
add_dependencies(router spdlog)
target_link_libraries(router ${WIRINGPI_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} rt crypt)