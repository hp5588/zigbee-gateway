#include <iostream>
#include <src/CoordinatorApp.h>
#include <spdlog/spdlog.h>
#include <src/RouterApp.h>


using namespace std;




int main() {
    auto console = spdlog::stdout_color_mt("main");

    spdlog::set_level(spdlog::level::debug);
    /*int fd = serialOpen("/dev/tty0",115200);

    if (fd){
        cout<< "fd:" + fd << endl;
    } else{
        return -1;
    }

    wiringPiSetup();
    pinMode(4,OUTPUT);
    wiringPiISR(5, INT_EDGE_FALLING ,dataReadyCallback);*/


    console->info("App Starting");
    RouterApp *app = new RouterApp();
    app->run();




    /*    cout << "Init" <<endl;
        ZigbeeZnp *znp = new ZigbeeZnp(DeviceBase::Coordinator,0x0012);
        Coordinator *coordinator = (Coordinator *) znp->open();*/
    //    coordinator->scan();

    //    Router *router = (Router *) znp->open();

    console->info("App Stopped");

    return 0;
}
