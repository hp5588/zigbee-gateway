//
// Created by brian on 5/11/17.
//

#include "AppBase.h"


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
template<class T>
void AppBase<T>::rxThread() {
    logger->debug("Rx thread begin");
    while (true){
        Response rsp = device->readResponse();

        logger->debug("response received -> type: " + std::to_string(rsp.getMsgType())
                      + " sub:" + std::to_string(rsp.getSubSys())
                      + " cmdId: " + std::to_string(rsp.getCmdId())
        );

        switch (rsp.getMsgType()){
            case SRSP:{
                lockSyncRspsMutex();
                syncRsps.push_front(rsp);
                releaseSyncRspsMutex();
                break;
            }
            case AREQ:{
                switch (rsp.getSubSys()){
                    case SYS:{
                        sysAreqHandler(&rsp);
                        break;
                    }
                    case SimpleAPI:{
                        sapiAreqHandler(&rsp);
                        break;
                    }
                    case RPCError:break;
                    case RESERVED0:break;
                    case RESERVED1:break;
                    case AF:
                        afAreqHandler(&rsp);
                        break;
                    case ZDO:{
                        zdoAreqHandler(&rsp);
                        break;
                    }
                    case UTIL:break;
                    case RESERVED2:break;
                    case APP:break;
                    case APP_CONFIG:
                        appConfigAreqHandler(&rsp);
                        break;
                    case GREEN_POWER:
                        break;
                }
                break;
            }
        }
//        std::this_thread::sleep_for(std::chrono::milliseconds(1));

    }
}
template<class T>
Response *AppBase<T>::readSrsp() {
    if (!syncRsp.isRead()){
        syncRsp.setRead(true);
        return &syncRsp;
    }
    return nullptr;
}

template<class T>
bool AppBase<T>::lockSyncRspsMutex() {
    this->syncRspsMutex.lock();
    return true;
}

template<class T>
bool AppBase<T>::releaseSyncRspsMutex() {
    this->syncRspsMutex.unlock();
    return true;
}


template<class T>
AppBase<T>::AppBase() {}


template class AppBase<Coordinator>;
template class AppBase<Router>;


#pragma clang diagnostic pop
