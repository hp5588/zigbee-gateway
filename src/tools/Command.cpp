//
// Created by brian on 4/4/17.
//

#include <cstring>
#include <cstdlib>
#include "Command.h"
void Command::finalizeRawData() {
    this->rawData[0] = PACKET_SOF;
    this->rawData[packetSize-1] = calcFCS(&rawData[1], (unsigned char) (packetSize - 2));
/*    packet->sof = PACKET_SOF;
    packet->fcs = calcFCS(&this->rawData[1], (unsigned char) (packet->length + 3));*/
}

unsigned char Command::calcFCS(unsigned char *pMsg, unsigned char len) {
    unsigned char result = 0;   while (len--)   {
        result ^= *pMsg++;
    }
    return result;
}

Command::Command(MsgType type, SubSys sys, uint8_t cmdID, uint8_t *data, uint8_t length) {
    this->type = type;
    this->subSys = sys;
    this->cmdId = cmdID;
    this->data = data;
    this->length = length;
    generatePacket();
}

void Command::generatePacket() {
    UartPacket *uartPacket = new UartPacket();
    uartPacket->cmd0 = type << 5 | subSys;
    uartPacket->cmd1 = cmdId;
    uartPacket->length = length;
    uartPacket->data = data;


    /*copy to buffer*/
    packetSize = length + 5 ;//+ length - 1;
    this->rawData = (uint8_t *) malloc(packetSize);

    uint8_t *p = this->rawData;

    memcpy(p,uartPacket,4);
    p +=4;
    if (uartPacket->data) {
        memcpy(p, uartPacket->data, length);
        p += length;
    }

    finalizeRawData();

}

uint8_t Command::getPacketSize() const {
    return packetSize;
}

void Command::copy(uint8_t *dest, uint16_t length) {
    memcpy(dest,rawData,length);

}

uint8_t *Command::getRawDataP() const {
    return rawData;
}



