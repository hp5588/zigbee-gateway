//
// Created by brian on 4/5/17.
//

#ifndef ZIGBEE_COORDINATOR_RESPONSE_H
#define ZIGBEE_COORDINATOR_RESPONSE_H


#include <cstdint>
#include <src/define/CC2530.h>
#include <install/include/spdlog/logger.h>
#include <install/include/spdlog/spdlog.h>

class Response {
public:
    Response(uint8_t* rawData, uint16_t length);
    Response();

    uint16_t copy(uint8_t* dest, uint16_t length);

    MsgType getMsgType() const;

    SubSys getSubSys() const;

    uint8_t getCmdId() const;

    bool isRead() const;

    void setRead(bool read);

    bool isValid() const;

    uint8_t *getData() const;
    void debugPrint();

private:
    MsgType msgType;
    SubSys subSys;
    uint8_t cmdId;

    bool valid;
    bool read = false;

    uint8_t * rawData;
    uint16_t rawDatalength;

    UartPacket *uartPacket;
    uint8_t *data;

    bool parse();
    void append(std::string title, uint8_t *data, uint8_t length);
    std::shared_ptr<spdlog::logger> logger =  spdlog::get("Response");



public:


};


#endif //ZIGBEE_COORDINATOR_RESPONSE_H
