//
// Created by brian on 4/4/17.
//

#ifndef ZIGBEE_COORDINATOR_FRAME_H
#define ZIGBEE_COORDINATOR_FRAME_H


#include <cstdint>
#include <src/define/CC2530.h>

class Command {
public:
    Command(MsgType type, SubSys sys, uint8_t cmdID, uint8_t *data, uint8_t length);
    void copy(uint8_t* dest ,uint16_t length);

private:
    MsgType  type;
    SubSys subSys;
    uint8_t cmdId;
    uint8_t *data;
    uint8_t length;

    uint8_t *rawData;
    uint8_t packetSize;


public:
    uint8_t *getRawDataP() const;

private:
public:
    uint8_t getPacketSize() const;

private:
    void finalizeRawData();
    unsigned char calcFCS(unsigned char *pMsg, unsigned char len);
    void generatePacket();
};


#endif //ZIGBEE_COORDINATOR_FRAME_H
