//
// Created by brian on 4/5/17.
//

#include <cstdlib>
#include <cstring>
#include <src/define/MT.h>
#include <sstream>
#include <iomanip>
#include "Response.h"

Response::Response() {}

Response::Response(uint8_t *rawData, uint16_t length) {
    this->rawData = rawData;
    this->rawDatalength = length;
    if (parse()){
        this->valid = true;
    }
}

bool Response::parse() {
    uartPacket = new UartPacket;
    uint8_t *p = rawData;
    uartPacket->sof = *(p++);
    uartPacket->length = *(p++);
    uartPacket->cmd0 = *(p++);
    uartPacket->cmd1 = *(p++);
    uartPacket->data = p++;

/*    if (uartPacket->cmd0 == 0x60 && uartPacket->cmd1==0){
        *//*error*//*
    }*/


    this->data = (uint8_t *) malloc(uartPacket->length);
    p = this->data;
    memcpy(p,uartPacket->data,uartPacket->length);

    msgType = (MsgType) ((0xE0 & uartPacket->cmd0)>>5);
    subSys = (SubSys) (0x1F & uartPacket->cmd0);
    cmdId = uartPacket->cmd1;



    /*TODO check verification code*/
/*    uint8_t verfCode = *p;
    if (verfCode == )*/

    return true;
}

uint16_t Response::copy(uint8_t *dest, uint16_t length) {
    memcpy(dest,this->data,length);
    return length;
}

MsgType Response::getMsgType() const {
    return msgType;
}

SubSys Response::getSubSys() const {
    return subSys;
}

uint8_t Response::getCmdId() const {
    return cmdId;
}

bool Response::isValid() const {
    return valid;
}

uint8_t *Response::getData() const {
    return data;
}

bool Response::isRead() const {
    return read;
}

void Response::setRead(bool read) {
    Response::read = read;
}

void Response::debugPrint() {


    switch ((SubSys)subSys){

        case RPCError:break;
        case SYS:break;
        case RESERVED0:break;
        case RESERVED1:break;
        case AF:break;
        case ZDO:{
            switch (cmdId){
                case MT_ZDO_TC_DEVICE_IND:{

                    std::stringstream ss;
                    uint8_t position = 0;
                    ss << " SrcNwkAddr:" << std::hex ;
                    for (int i = 0; i < 2; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int)data[position++];
                    }
                    ss << " WasBroadcast:";
                    for (int i = 0; i < 8; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int)data[position++];
                    }

                    ss << " ParentNwkAddr:";
                    for (int i = 0; i < 2; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int)data[position++];
                    }

                    logger->debug(ss.str());


                }
            }

            break;
        }
        case SimpleAPI:break;
        case UTIL:{
            switch (cmdId){
                case MT_UTIL_GET_DEVICE_INFO:{
                    std::stringstream ss;
                    uint8_t position = 0;
                    ss << " status:" << std::hex << (int)data[position++];
                    ss << " IEEE address:";
                    for (int i = 0; i < 8; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int) data[position++];
                    }
                    ss << " Short address:" ;
                    for (int i = 0; i < 2; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int) data[position++];
                    }
                    ss << " Device Type:" << std::hex << (int) data[position++];
                    ss << " Device State:" << std::hex << (int) data[position++];

                    uint8_t length = data[position++];
                    ss << " NumAssocDevices:" << length;

                    ss << " AssocDevicesList:" ;
                    for (int i = 0; i < length; ++i) {
                        ss << std::setfill('0') << std::setw(2) << (int) data[position++];
                    }

                    logger->debug(ss.str());
                    break;
                }
            }
            break;
        }
        case RESERVED2:break;
        case APP:break;
        case APP_CONFIG:break;
        case GREEN_POWER:break;
    }
}

void Response::append(std::string title, uint8_t *data, uint8_t length) {
//    ss << title << ":" ;
//    for (int i = 0; i < length; ++i) {
//        ss << std::setfill('0') << std::setw(2) << (int) data++;
//    }
//    ss << ";";
}
