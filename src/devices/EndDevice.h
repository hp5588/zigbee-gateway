//
// Created by brian on 4/2/17.
//

#ifndef ZIGBEE_COORDINATOR_ENDDEVICE_H
#define ZIGBEE_COORDINATOR_ENDDEVICE_H

#include <cstdint>
#include "DeviceBase.h"

class EndDevice : public DeviceBase{
public:

    EndDevice(Role role, uint16_t panId, uint16_t deviceId);

    bool isConnected() override;

    void getNetAddr() override;



    bool init() override;


    bool send(uint16_t destAddr, uint8_t* data);
    void read(uint8_t* buffer, uint16_t length, uint16_t *senderAddr);

};


#endif //ZIGBEE_COORDINATOR_ENDDEVICE_H
