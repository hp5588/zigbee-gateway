//
// Created by brian on 4/4/17.
//


#include <cstring>
#include <wiringPi.h>
#include <thread>
#include <src/define/MT.h>
#include "DeviceBase.h"

DeviceBase::DeviceBase(Role role, uint16_t panId, uint16_t deviceId) : role(role) {
    this->role = role;
    this->panId = panId;
//    logger = spdlog::get("Device");
}

void DeviceBase::writeCommand(Command *command) {
/*    uint8_t rawData[sizeof(command->getPacketSize())];
    command->copy(rawData, command->getPacketSize());*/

    while (digitalRead(CT_PIN));
    uint8_t *dataP = command->getRawDataP();

/*

    void *buffer[100];
    char **strings;
    int nptrs = backtrace(buffer, 100);
    printf("backtrace() returned %d addresses\n", nptrs);
    strings = backtrace_symbols(buffer, nptrs);
    for (int j = 0; j < nptrs; j++)
        printf("%s\n", strings[j]);
*/


    //        logger->debug("fd:" + std::to_string(this->fd) );

    for (int i = 0; i < command->getPacketSize(); ++i) {
//        logger->debug("fd" + std::to_string(this->fd) +  " byte " + std::to_string(*dataP) );
        serialPutchar(this->fd, (const unsigned char) *dataP++);
    }

//    serialFlush(this->fd);

}

Response DeviceBase::readResponse() {

//    digitalWrite(RT_PIN,LOW);
    memset(this->rxBuffer,0x00, sizeof(this->rxBuffer));

//    while (digitalRead(RT_PIN));

    uint16_t count = 0;
    uint8_t length = 0;

    bool completeFlag = false;
    bool startFlag = false;
    while (!completeFlag){
//        uint16_t charAvaliable=(uint16_t) serialDataAvail(this->fd);
        if (startFlag && (count > length + 3)){
            completeFlag = true;
        } else {
            /*check if this is longer than message*/
//            if(count > length + 3)
//                break;

            uint8_t c = (uint8_t) serialGetchar(this->fd);
            if (!startFlag) {
                if (c == PACKET_SOF) {
                    startFlag = true;
                } else { continue; }
            }

            if (count == 1)
                length = c;

            rxBuffer[count++] = c;

        }
    }

/*    logger->debug("raw data length: " + std::to_string(length));
    logger->debug("raw data count: " + std::to_string(count));*/


    Response response(this->rxBuffer, count);
    return response;

}

unsigned char DeviceBase::calcFCS(unsigned char *pMsg, unsigned char len) {
    unsigned char result = 0;   while (len--)   {
        result ^= *pMsg++;
    }
    return result;
}



bool DeviceBase::initNetwork(uint16_t panId) {


    Command *command;
    Response response = Response();


    /*sof reset*/
/*
    uint8_t rstData[1] = {0};
    command = new Command(AREQ, SYS, MT_SYS_RESET_REQ, (uint8_t *) rstData, sizeof(rstData));
    this->writeCommand(command);
    response = this->readResponse();
*/
    /*set channel list value*/
    uint8_t channelData[] = {ZCD_NV_CHANLIST,0x04 ,0x00 ,0x00, 0x00, 0x02};
    command = new Command(SREQ, SimpleAPI, ZB_WRITE_CONFIGURATION, (uint8_t *) channelData, sizeof(channelData));
    this->writeCommand(command);
    response = this->readResponse();



    /*set PAN ID*/
    uint8_t idData[] = {ZCD_NV_PANID, 0x02,(uint8_t) (panId & 0x00FF), (uint8_t) (panId >> 8) };
    command = new Command(SREQ, SimpleAPI, ZB_WRITE_CONFIGURATION, (uint8_t *) idData, sizeof(idData));
    writeCommand(command);
    response = readResponse();

    sysHardReset();

    /*for new network parameters to take effect*/
//    resetNetworkState();

    /*register app using simple API*/
//    uint8_t appData[] = {  APP_END_POINT,
//                           APP_PROFILE_ID >> 8, APP_PROFILE_ID & 0x00FF,
//                           (uint8_t) (this->deviceId >> 8), (uint8_t) (0x00FF & deviceId),
//                           DEVICE_VERSION,
//                           0x00,
//                           0x01,
//                           REALAY_CONTROL_INPUT_COMMAND_IDF >> 8, REALAY_CONTROL_INPUT_COMMAND_IDF & 0x00FF,
//                           0x01,
//                           REALAY_CONTROL_OUTPUT_COMMAND_IDF >> 8, REALAY_CONTROL_OUTPUT_COMMAND_IDF & 0x00FF,
//    };
//    command = new Command(SREQ, SimpleAPI, ZB_APP_REGISTER_REQUEST, (uint8_t *) appData, sizeof(appData));
//    writeCommand(command);
//    response = readResponse();


    /*AF register*/
    uint8_t afData[] = {
            APP_END_POINT,
            APP_PROFILE_ID >> 8, APP_PROFILE_ID & 0x00FF,
            (uint8_t) (this->deviceId >> 8), (uint8_t) (0xFF & deviceId),
            DEVICE_VERSION,
            0x00, //no latency
            0x01, //AppNumInClusters
            0x00,0x01, //AppInClusterList
            0x01, // AppOutClusterList
            0x00,0x02//AppOutClusterList
    };
    command = new Command(SREQ, AF, MT_AF_REGISTER, (uint8_t *) afData, sizeof(afData));
    writeCommand(command);
    response = readResponse();

    /*start request*/
//    command = new Command(SREQ, SimpleAPI, MT_SAPI_START_REQ, nullptr, 0);
//    writeCommand(command);
//    response = readResponse();




    /*ignore state changes*/

    /*wait for ZB_START_CONFIRM*/
//    if (role == COORDINATOR) {
//        bool ready = false;
//        while (!ready) {
//            response = readResponse();
//            if (response.getSubSys() == SimpleAPI) {
//                if (response.getCmdId() == MT_SAPI_START_CNF) {
//                    if (response.getData()[0] == 0) {
//                        ready = true;
//                    }
//                }
//            }
//            usleep(1000);
//        }
//    }

    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    serialFlush(fd);
    logger->debug("network initialized");
}

bool DeviceBase::initRole(DeviceBase::Role role) {
    /*set role*/
    uint8_t data[] = {ZCD_NV_LOGICAL_TYPE, 0x01, role};
    Command *command = new Command(SREQ, SimpleAPI, ZB_WRITE_CONFIGURATION, (uint8_t *) data, sizeof(data));
    this->writeCommand(command);
    Response response = this->readResponse();
    if (response.isValid()){
        if (response.getSubSys() == RPCError){
//            command error
            return false;
        }
        uint8_t status = response.getData()[0];
        switch (status){
            case ZSuccess:{
                break;
            }
            case ZFailure:{
                return false;
            }
            default:break;
        }


    }
    logger->debug("role initialized");


}

void DeviceBase::initIO() {
    logger = spdlog::stdout_color_mt("Device");

    fd = serialOpen("/dev/serial0",115200);
    if (fd<0){
        logger->warn("Serial Open Failed");
        return;
    }
    /*clear buffer*/
    serialFlush(fd);

    wiringPiSetup();

    pinMode(REST_PIN,OUTPUT);
    pinMode(CFG0_PIN,OUTPUT);
    pinMode(CFG1_PIN,OUTPUT);
    pinMode(CT_PIN,INPUT);
    pinMode(RT_PIN,OUTPUT);


    /*rest cc2530 by hw pin */
    digitalWrite(REST_PIN,LOW);

    /*configure pin*/
    digitalWrite(CFG0_PIN,LOW);    //32kHz crystal present
    digitalWrite(CFG1_PIN,LOW);     //use UART interface

    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    /*start-up cc2530*/
    digitalWrite(REST_PIN,HIGH);

    /*ready to receive*/
    digitalWrite(RT_PIN,LOW);


}

DeviceBase::DeviceBase() {}

void DeviceBase::getDeviceInfo() {

    /*TODO print for debugging*/
    for (int i = 0; i < 8; ++i) {
        Command *command = new Command(SREQ, UTIL, MT_UTIL_GET_DEVICE_INFO, (uint8_t *) nullptr, 0);
        writeCommand(command);
    }

}

void DeviceBase::resetNetworkState() {

    sysHardReset();

    uint8_t idData[] = {ZCD_NV_STARTUP_OPTION ,0x01 ,0x83 };
//    uint8_t idData[] = {ZCD_NV_STARTUP_OPTION ,0x01 ,0x02 };
    Command *command = new Command(SREQ, SimpleAPI, ZB_WRITE_CONFIGURATION, (uint8_t *) idData, sizeof(idData));
    writeCommand(command);
    Response response = readResponse();

    /*reset to take effect*/
    sysHardReset();

    logger->debug("network state reset");

    return;
}

bool DeviceBase::broadcast() {
    static uint8_t handle = 0;
    uint8_t data[9] = { 0xFF,0xFF,
                        RELAY_COMMAND_ID >> 8, RELAY_COMMAND_ID & 0xFF,
                        handle++,
                        0,
                        10,
                        0x01,
                        0xAA
    };
    Command *command = new Command(SREQ, SimpleAPI, MT_SAPI_SEND_DATA_REQ, (uint8_t *) data, sizeof(data));
    this->writeCommand(command);


    logger->debug("Broadcast");
    return true;
}

void DeviceBase::scan() {
    uint8_t searchKey[] = { 0x05 };
    Command *command = new Command(SREQ, SimpleAPI, MT_SAPI_FIND_DEV_REQ, (uint8_t *) searchKey, sizeof(searchKey));
    this->writeCommand(command);
}

void DeviceBase::bindDevices() {
    /*always allow bind*/
    uint8_t bindData[] = {(uint8_t) true,
                          0x00, 0x01, // identifier
             0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
    };
    Command *command = new Command(SREQ, SimpleAPI, MT_SAPI_BIND_DEVICE_REQ, bindData, sizeof(bindData));
    writeCommand(command);
}

void DeviceBase::allowBinding() {
    uint8_t timeout[] = {0x65}; //always true
    Command *command = new Command(SREQ, SimpleAPI, MT_SAPI_ALLOW_BIND_REQ, timeout, sizeof(timeout));
    writeCommand(command);
}

void DeviceBase::permitJoin() {
    uint8_t cfg[] = {
            0xFF,0xFC,  //all devices
            0xFF        //always permit
    };
    Command *command = new Command(SREQ, SimpleAPI, MT_SAPI_PMT_JOIN_REQ, cfg, sizeof(cfg));
    writeCommand(command);

}


void DeviceBase::sendData(uint16_t dstAddr, uint8_t *data, uint8_t length) {
    uint8_t cfgData[] = {
            (uint8_t) (dstAddr >> 8), (uint8_t) (dstAddr & 0xFF),
            APP_END_POINT,
            APP_END_POINT,
            0x00, 0x02,
            0xAA,
            0x20,
            5,
            length
    };
    uint8_t *fnlData = (uint8_t *) malloc(length + 10);
    memcpy(fnlData,cfgData,10);
    memcpy(fnlData+10,data,length);

    Command *command = new Command(SREQ, AF, MT_AF_DATA_REQUEST, fnlData, (uint8_t) (length + 10));
    writeCommand(command);
}

void DeviceBase::sysHardReset() {
    uint8_t resetData[] = {0x00};
    Command *command = new Command(AREQ, SYS, MT_SYS_RESET_REQ, (uint8_t *) resetData, sizeof(resetData));
    writeCommand(command);
    Response response = readResponse();
    logger->debug("watch-dog reset");
}

void DeviceBase::appSetChannel(uint8_t channelNum, bool isPrimary) {
    uint32_t channel = (uint32_t) (0x00000001 << channelNum);
    uint8_t channelData[] = {
            (uint8_t) isPrimary,
            (uint8_t) (channel & 0xFF),
            (uint8_t) (channel >> 8),
            (uint8_t) (channel >> 16),
            (uint8_t) (channel >> 24),
    };
    Command *command = new Command(AREQ, APP_CONFIG, MT_APP_CNF_BDB_SET_CHANNEL, (uint8_t *) channelData, sizeof(channelData));
    writeCommand(command);
//    logger->info("set channel to "+ std::to_string(channel));
//    logger->debug("MT_APP_CNF_BDB_SET_CHANNEL -> status:"+ std::to_string(response.getData()[0]));

}



void DeviceBase::appStartCommission(uint8_t commissionMode) {
    uint8_t cfg[] = {
            commissionMode
    };
    Command *command = new Command(SREQ, APP_CONFIG, MT_APP_CNF_BDB_START_COMMISSIONING, cfg, sizeof(cfg));
    writeCommand(command);
}

