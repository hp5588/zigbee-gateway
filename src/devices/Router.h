//
// Created by brian on 4/4/17.
//

#ifndef ZIGBEE_COORDINATOR_ROUTOR_H
#define ZIGBEE_COORDINATOR_ROUTOR_H


#include <cstdint>
#include "DeviceBase.h"

class Router : public DeviceBase {
public:
    Router();
    Router(Role role, const uint16_t &panId, uint16_t deviceId);

private:
    bool isConnected() override;

    void getNetAddr() override;


    bool init() override;

};


#endif //ZIGBEE_COORDINATOR_ROUTOR_H
