//
// Created by brian on 5/23/17.
//

#ifndef ZIGBEE_COORDINATOR_DEVICEMODEL_H
#define ZIGBEE_COORDINATOR_DEVICEMODEL_H


#include <cstdint>
#include <src/tools/Response.h>

class DeviceModel {
public:
    DeviceModel(uint64_t ieeeAddr, uint16_t shortAddr, uint16_t parentAddr);
    DeviceModel(Response &response);

    uint64_t getIeeeAddr() const;
    void setIeeeAddr(uint64_t ieeeAddr);

    uint16_t getShortAddr() const;

    void setShortAddr(uint16_t shortAddr);

    uint16_t getParentAddr() const;

    void setParentAddr(uint16_t parentAddr);

private:
    uint64_t ieeeAddr;
    uint16_t shortAddr;
    uint16_t parentAddr;
};


#endif //ZIGBEE_COORDINATOR_DEVICEMODEL_H
