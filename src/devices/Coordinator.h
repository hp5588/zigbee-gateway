//
// Created by brian on 4/2/17.
//

#ifndef ZIGBEE_COORDINATOR_COORDINATOR_H
#define ZIGBEE_COORDINATOR_COORDINATOR_H


#include <cstdint>
#include "DeviceBase.h"

class Coordinator : public DeviceBase {
public:
    Coordinator();
    Coordinator(DeviceBase::Role role, uint16_t panId, uint16_t deviceId);

    bool isConnected() override;

    void getNetAddr() override;

    bool init() override;


private:
//    bool initNetwork(uint16_t panId);


};


#endif //ZIGBEE_COORDINATOR_COORDINATOR_H
