//
// Created by brian on 5/23/17.
//

#include <src/define/MT.h>
#include "DeviceModel.h"

uint64_t DeviceModel::getIeeeAddr() const {
    return ieeeAddr;
}

void DeviceModel::setIeeeAddr(uint64_t ieeeAddr) {
    DeviceModel::ieeeAddr = ieeeAddr;
}

uint16_t DeviceModel::getShortAddr() const {
    return shortAddr;
}

void DeviceModel::setShortAddr(uint16_t shortAddr) {
    DeviceModel::shortAddr = shortAddr;
}

uint16_t DeviceModel::getParentAddr() const {
    return parentAddr;
}

void DeviceModel::setParentAddr(uint16_t parentAddr) {
    DeviceModel::parentAddr = parentAddr;
}

DeviceModel::DeviceModel(uint64_t ieeeAddr, uint16_t shortAddr, uint16_t parentAddr) : ieeeAddr(ieeeAddr),
                                                                                       shortAddr(shortAddr),
                                                                                       parentAddr(parentAddr) {
    this->ieeeAddr = ieeeAddr;
    this->shortAddr = shortAddr;
    this->parentAddr = parentAddr;
}

DeviceModel::DeviceModel(Response &response) {
    if (response.getCmdId() != MT_ZDO_TC_DEVICE_IND)
        return ;
    if (response.getSubSys() != ZDO)
        return ;

    uint8_t count = 0;

    for (int i = 0; i < 2; ++i) {
        ((uint8_t *)(&shortAddr))[1-i] = response.getData()[count++];
    }
    for (int i = 0; i < 8; ++i) {
        ((uint8_t *)(&ieeeAddr))[7-i] = response.getData()[count++];
    }
    for (int i = 0; i < 2; ++i) {
        ((uint8_t *)(&parentAddr))[1-i] = response.getData()[count++];
    }


}
