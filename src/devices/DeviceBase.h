//
// Created by brian on 4/2/17.
//

#ifndef ZIGBEE_COORDINATOR_DEVICE_H
#define ZIGBEE_COORDINATOR_DEVICE_H

#include <list>
#include <wiringSerial.h>
#include <cstdint>
#include <../define/CC2530.h>
#include <src/tools/Response.h>
#include <src/tools/Command.h>
#include <install/include/spdlog/spdlog.h>
#include <AppParms.h>



class DeviceBase {
public:
    std::shared_ptr<spdlog::logger> logger;

    virtual bool isConnected() = 0;
    virtual void getNetAddr() = 0;
    virtual bool init() = 0;

    enum Role {
        Coordinator,
        Router,
        Endpoint
    };

    DeviceBase();

    DeviceBase(Role role, uint16_t panId, uint16_t deviceId);

    Response readResponse();

    bool broadcast();
    void scan();
    void bindDevices();
    void allowBinding();
    void permitJoin();
    void appStartCommission(uint8_t commissionMode);
    void appSetChannel(uint8_t channel, bool isPrimary);
    void sendData(uint16_t dstAddr, uint8_t *data, uint8_t length);
    void getDeviceInfo();


protected:
    Role role;
    int fd;
    uint16_t panId;
    uint16_t deviceId;
    static const uint8_t REST_PIN = 0x01;
    static const uint8_t CFG0_PIN = 0x04;
    static const uint8_t CFG1_PIN = 0x05;
    static const uint8_t CT_PIN = 0x00;
    static const uint8_t RT_PIN = 0x02;

    void writeCommand(Command *command);
    unsigned char calcFCS(unsigned char *pMsg, unsigned char len);

    void initIO();
    bool initNetwork(uint16_t panId);
    bool initRole(Role role);



    void resetNetworkState();
    void sysHardReset();


private:
    uint8_t rxBuffer[256];


public:

    Role getRole() const {
        return role;
    }

};




#endif //ZIGBEE_COORDINATOR_DEVICE_H
