//
// Created by brian on 4/2/17.
//

#include <src/tools/Command.h>
#include <src/define/MT.h>
#include <src/define/AppParms.h>
#include "Coordinator.h"

bool Coordinator::isConnected() {
    return false;
}

void Coordinator::getNetAddr() {

}


bool Coordinator::init() {


    initIO();

    /*wait for SYS_RESET_IND*/
    Response rstIndResponse = readResponse();

    resetNetworkState();

    initRole(Role::Coordinator);
    initNetwork(panId);


}

Coordinator::Coordinator(DeviceBase::Role role, uint16_t panId, uint16_t deviceId) : DeviceBase(role, panId, deviceId) {
    this->role = role;
    this->panId = panId;
    this->deviceId = deviceId;
}


Coordinator::Coordinator() {

}






