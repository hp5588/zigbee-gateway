//
// Created by brian on 4/4/17.
//

#include "Router.h"
#include "DeviceBase.h"


bool Router::isConnected() {
    return false;
}

void Router::getNetAddr() {

}


bool Router::init() {
    initIO();
    Response rstIndResponse = readResponse();

    resetNetworkState();

    initRole(role);
    initNetwork(panId);
}

Router::Router(Role role, const uint16_t &panId, uint16_t deviceId) : DeviceBase(role, panId, deviceId) {
    this->role = role;
    this->panId = panId;
    this->deviceId = deviceId;
}


Router::Router() {

}
