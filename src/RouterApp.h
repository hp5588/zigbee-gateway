//
// Created by brian on 5/11/17.
//

#ifndef ZIGBEE_COORDINATOR_ROUTERAPP_H
#define ZIGBEE_COORDINATOR_ROUTERAPP_H


#include <src/devices/Router.h>
#include "AppBase.h"

class RouterApp : public AppBase<Router> {
public:
    RouterApp();
    void run() override;

private:
    bool init() override;
    void sysAreqHandler(Response *response) override;
    void sapiAreqHandler(Response *response) override;
    RouterApp( const RouterApp& other ) = delete;
    RouterApp& operator=( const RouterApp& ) = delete;

protected:
    void zdoAreqHandler(Response *response) override;

    void appConfigAreqHandler(Response *response) override;

    void afAreqHandler(Response *response) override;
};



#endif //ZIGBEE_COORDINATOR_ROUTERAPP_H
