//
// Created by brian on 4/2/17.
//

#ifndef ZIGBEE_COORDINATOR_CC2530_ZNP_H
#define ZIGBEE_COORDINATOR_CC2530_ZNP_H


enum MsgType {
    POLL,
    SREQ,
    AREQ,
    SRSP
};
enum SubSys{
    RPCError,                   //0
    SYS,                        //
    RESERVED0,                  //
    RESERVED1,                  //
    AF,                         //
    ZDO,                        //
    SimpleAPI,                  //
    UTIL,                       //
    RESERVED2,                  //
    APP,                         //
    APP_CONFIG = 0x0F,
    GREEN_POWER = 0x15

};

struct UartPacket{
    uint8_t sof;
    uint8_t length;
    uint8_t cmd0;
    uint8_t cmd1;
    uint8_t *data;
    uint8_t fcs;
};

/*devices specific configuration*/
#define PACKET_SOF 0xFE

#define ZCD_NV_STARTUP_OPTION 0x0003
#define ZCD_NV_LOGICAL_TYPE  0x0087
#define ZCD_NV_ZDO_DIRECT_CB  0x008F


/*network specific configuration*/
#define ZCD_NV_PANID  0x0083


/*response*/
enum ErrorCode{
    Undefined,        //0x00
    InvalidSubsystem, //0x01
    InvalidCommandID, //0x02
    InvalidParameter, //0x03
    InvalidLength     //0x04
};


#endif //ZIGBEE_COORDINATOR_CC2530_ZNP_H
