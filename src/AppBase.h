//
// Created by brian on 5/11/17.
//

#ifndef ZIGBEE_COORDINATOR_APPBASE_H
#define ZIGBEE_COORDINATOR_APPBASE_H

#include <install/include/spdlog/logger.h>
#include <src/tools/Response.h>
#include <src/devices/DeviceBase.h>
#include <src/devices/Coordinator.h>
#include <src/devices/Router.h>
#include <src/define/AppParms.h>
#include <deque>
#include <src/devices/DeviceModel.h>


template <class T>
class AppBase {
public:
    virtual void run() = 0;


protected:
    std::shared_ptr<spdlog::logger> logger;
    Response syncRsp;
    std::deque<Response> syncRsps;

    DeviceBase::Role role;
    T *device;

    std::vector<DeviceModel> networkDevicies;

    virtual bool init() = 0;
    virtual void sysAreqHandler(Response *response) = 0;
    virtual void sapiAreqHandler(Response *response) = 0;
    virtual void zdoAreqHandler(Response *response) = 0;
    virtual void appConfigAreqHandler(Response *response) = 0;
    virtual void afAreqHandler(Response *response) = 0;
    void rxThread();
    Response * readSrsp();

    bool lockSyncRspsMutex();
    bool releaseSyncRspsMutex();

    std::mutex syncRspsMutex;

    bool ready = false;

private:
public:
    AppBase();
    AppBase( const AppBase& other ) = delete;
    AppBase& operator=( const AppBase& ) = delete;

};



#endif //ZIGBEE_COORDINATOR_APPBASE_H
