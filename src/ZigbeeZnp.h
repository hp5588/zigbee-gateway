//
// Created by brian on 4/2/17.
//

#ifndef ZIGBEE_COORDINATOR_ZIGBEEZNP_H
#define ZIGBEE_COORDINATOR_ZIGBEEZNP_H


#include <stdint-gcc.h>
#include <src/devices/DeviceBase.h>
#include "src/define/CC2530.h"

class ZigbeeZnp {

private:


    DeviceBase::Role role;
    uint16_t panId;

public:
    ZigbeeZnp(DeviceBase::Role role, uint16_t panId);

    DeviceBase * open();






};


#endif //ZIGBEE_COORDINATOR_ZIGBEEZNP_H
