//
// Created by brian on 4/2/17.
//

#include <src/devices/Coordinator.h>
#include <src/devices/EndDevice.h>
#include <src/devices/Router.h>
#include "ZigbeeZnp.h"




ZigbeeZnp::ZigbeeZnp(DeviceBase::Role role, uint16_t panId) {
    this->role = role;
    this->panId = panId;
}

DeviceBase* ZigbeeZnp::open() {

    DeviceBase *device = nullptr;
    switch (role){
        case DeviceBase::Coordinator:{
            device = new Coordinator(DeviceBase::Coordinator, panId, 0x0020);
            break;
        }
        case DeviceBase::Endpoint:{
            device = new EndDevice(DeviceBase::Endpoint, panId, 0x0060);
            break;
        }
        case DeviceBase::Router:{
            device = new Router(DeviceBase::Router, panId, 0x0040);
            break;
        }
    }
    if (device){
        device->init();
    }
    return device;



}


