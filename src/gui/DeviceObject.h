#ifndef DEVICEOBJECT_H
#define DEVICEOBJECT_H
#include <QDebug>
#include <QObject>


class DeviceObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
    Q_PROPERTY(DeviceType deviceType READ deviceType WRITE setDeviceType NOTIFY deviceTypeChanged)
    Q_PROPERTY(int x READ x WRITE setX NOTIFY xChanged)
    Q_PROPERTY(int y READ y WRITE setY NOTIFY yChanged)
    Q_PROPERTY(uint16_t shortAddr READ shortAddr WRITE setShortAddr NOTIFY shortAddrChanged)

    Q_ENUMS(DeviceType)

public:
    enum DeviceType{
        Motor,
        WaterTank
    };

    DeviceObject(const QString &m_deviceName, DeviceType m_deviceType, int x, int y);

    explicit DeviceObject(QObject *parent = 0);
    QString deviceName() const {
        return m_deviceName;
    }
    DeviceType deviceType() const {
        return m_deviceType;
    }
    int x() const {
        return m_x;
    }
    int y() const {
        return m_y;
    }
    uint16_t shortAddr() const {
        return m_shortAddr;
    }

    void setDeviceName(const QString &a) {
        if (a != m_deviceName) {
            m_deviceName = a;
            emit deviceNameChanged();
            qDebug() << m_deviceName;
        }
    }

    void setDeviceType(const DeviceType &a) {
        if (a != m_deviceType) {
            m_deviceType = a;
            emit deviceTypeChanged();
            qDebug() << m_deviceType;
        }
    }

    void setX(const int &x) {
        if (x != m_x) {
            m_x = x;
            emit xChanged();
            qDebug() << m_x;
        }
    }

    void setY(const int &y) {
        if (y != m_y) {
            m_y = y;
            emit xChanged();
            qDebug() << m_y;
        }
    }
    void setShortAddr(const uint16_t &shortAddr) {
        if (shortAddr != m_shortAddr) {
            m_shortAddr = shortAddr;
            emit shortAddrChanged();
            qDebug() << m_shortAddr;
        }
    }
//    void set



private:
    QString m_deviceName;
    DeviceType m_deviceType;
    uint16_t m_shortAddr;
    int m_x;
    int m_y;

signals:
    void deviceNameChanged();
    void deviceTypeChanged();
    void xChanged();
    void yChanged();
    void shortAddrChanged();


};

#endif //DEVICEOBJECT_H
