import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.0

ApplicationWindow {
    id:root
    visible: true
    width: 800
    height: 480
    title: qsTr("Hello World")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        Component.onCompleted: contentItem.interactive = false

        z:0

        Flipable{
             id: flipable
             property bool flipped: false
             front:DeviceMap{
                 objectName: "deviceMap"
                anchors.fill: parent

            }
             back:DeviceDetail{
                 anchors.fill: parent
             }

             transform: Rotation {
                 id: rotation
                 origin.x: flipable.width/2
                 origin.y: flipable.height/2
                 axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
                 angle: 0    // the default angle
             }

             states: State {
                 name: "back"
                 PropertyChanges { target: rotation; angle: 180 }
                 when: flipable.flipped
             }

             transitions: Transition {
                 NumberAnimation { target: rotation; property: "angle"; duration: 500 }
             }

        }

//        DevicePage {
//            x: 0
//            y: 0
//        }


        SettingPage{

        }

//        Page {
//            Label {
//                text: qsTr("Second page")
//                anchors.centerIn: parent
//            }
//        }

    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        TabButton {
            text: qsTr("Devices")
        }
        TabButton {
            text: qsTr("Setting")
        }
    }

    MouseArea {
       anchors.fill: parent
       enabled: false
       cursorShape: Qt.BlankCursor
     }
}
