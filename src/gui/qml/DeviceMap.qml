import QtQuick 2.0
Item {
    anchors.fill: parent

    Rectangle{
        id:deviceMap
        anchors.fill: parent
        antialiasing: true
        smooth: true
        Behavior on scale { NumberAnimation { duration: 200 } }
                        Behavior on x { NumberAnimation { duration: 200 } }
                        Behavior on y { NumberAnimation { duration: 200 } }


        Image {
            id: map
//                width: 1200
//                height: 800
            source: "qrc:/img/hospital-floor-plan.png"
        }
        IconMap{
            id: iconMap
            dataSource: listModel
            anchors.fill: map
            z:10
        }

        PinchArea{

            anchors.fill:parent
            pinch.target: deviceMap
            z:5
//            pinch.minimumRotation: -360
//            pinch.maximumRotation: 360
            pinch.minimumScale: 0.5
            pinch.maximumScale: 5
            pinch.dragAxis: Pinch.XandYAxis

            property real zRestore: 0
            onPinchStarted: {
                console.debug("pinch started");
            }
            
        }


    }






}
