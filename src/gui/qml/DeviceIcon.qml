import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: deviceIcon
    objectName: "deviceIcon"
    property alias deviceIcon: deviceIcon

    property int index :-1
    width: parent.height*0.15
    height: parent.height*0.15

    signal iconClicked(int index)

    DropShadow {
         id: rectShadow
         anchors.fill: source
         cached: true
         horizontalOffset: 0
         verticalOffset: 0
         radius: 8.0
         samples: 16
         color: "#80000000"
         smooth: true
         source: deviceIcon
     }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            iconClicked(index);
            flipable.flipped = true;
        }
    }
    Rectangle{
        anchors.fill: parent
//        color: "#1F3547"
        color: "#152E42"
        radius: 10
        antialiasing: true
         border {
             width: 3
             color: "#3C6D9E"
         }
    }

    Image{
        anchors{
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }

        width: parent.width*0.8
        height: parent.height*0.8
//        anchors.fill: parent
        source: "qrc:/img/car-engine.svg"
    }


}
