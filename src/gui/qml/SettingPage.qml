import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.3
Item {
    id: settingPage
    objectName: "settingPage"
    signal textFieldChanged(string text);
    //    Row {
    //        id: settingAreaRow
    //        height: 300
    //        width: Window.width
    //        spacing: 15

    //        Rectangle{
    //            color: "#21aca6"
    //            anchors.fill: parent

    //        }

    //        Row {
    //            id: row
    //            height: 80
    //            anchors.left: parent.left
    //            anchors.leftMargin: -1
    //            anchors.right: parent.right
    //            anchors.rightMargin: -1

    //            Switch {
    //                id: switch1
    //                text: qsTr("Allow Join")
    //                anchors.left: parent.left
    //                anchors.leftMargin: 10
    //                z: 1
    //                checked: true
    //                anchors.verticalCenter: parent.verticalCenter
    //            }
    //        }

    //        Row {
    //            id: row1
    //            height: 400
    //            anchors.left: parent.left
    //            anchors.leftMargin: 0
    //            anchors.right: parent.right
    //            anchors.rightMargin: 0
    //            anchors.top: row.bottom
    //            anchors.topMargin: 0

    //            TextField {
    //                id: textField
    //                text: qsTr("Text Field")
    //                onTextChanged: {
    //                    settingPage.textFieldChanged(textField.text);
    //                    myObject.deviceName = textField.text;

    //                }
    //            }
    //        }
    //    }

    ColumnLayout {
        id: areaLayout
        anchors.fill: parent
        //        height: parent.height
        //        width: parent.width
        //        width: 100
        //        height: 100

        Rectangle{
            id: settingArea
            color: "#21aca6"
            height: parent.height*0.8
            width: parent.width

            ColumnLayout {
                id: columnLayout
                height: parent.height
                Row {
                    id: row
                    height: 80
                    width: parent.width

                    Switch {
                        id: switch1
                        text: qsTr("Allow Join")
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        z: 1
                        checked: true
                        anchors.verticalCenter: parent.verticalCenter
                    }
                }
            }
        }

        Rectangle{
            id: resetArea
            width: parent.width
            height: parent.height*0.2
            color: "#d1e622"

            Row {
                id: rstRow
                height: 80
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.left: parent.left
                anchors.leftMargin: 10

                Button {
                    id: button

                    text: qsTr("Reset Network")
                    highlighted: true
                    anchors.verticalCenter: parent.verticalCenter
                }

                Label {
                    id: label
                    text: qsTr("WARNING! This will result in DESTROY whole zigbee network")
                    anchors.left: button.right
                    anchors.leftMargin: 20
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }
    }


}
