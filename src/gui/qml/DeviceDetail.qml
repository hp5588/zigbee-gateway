import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2


Item {
    id: devicePage
    objectName: "devicePage"
    property alias titleLabel: titleLabel
    property variant device:currentDevice;
    property int index: currentIndex;


//    signal itemTouched(int index);
    signal motorStateChanged(int index, bool active);


    onDeviceChanged: {
        //update view
        titleLabel.text = currentDevice.deviceName
    }


    ColumnLayout {
        id: deviceDetailLayout
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.leftMargin: 0

        Row {
            id: topRow
            height: 50
            Layout.minimumHeight: 50
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillWidth: true

            Label {
                id: titleLabel
                objectName: "titleLabel"
                width: 80
                text: "This is a Device"
                anchors.verticalCenter: parent.verticalCenter
                verticalAlignment: Text.AlignVCenter
                //            text: qsTr("Please Select a device")

                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 14
            }

            Button {
                id: finishButton
                text: qsTr("Finish")
                height: parent.height*0.8
                width: 80
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 20
                anchors.right: parent.right
                onClicked: {
                    flipable.flipped = false
                }
            }
        }

        RowLayout {
            id: rowLayout
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            height: parent.height-topRow.height

            Column {
                id: column
                width: Window.width/3
                Layout.minimumWidth: 200
                Layout.fillHeight: true
//                Rectangle{
//                    anchors.fill: parent
//                    color: "#006666"
//                }

                Switch {
                    id: switch1
                    anchors.centerIn: parent
                    text: qsTr("Switch")
                    onCheckedChanged: {
                        motorStateChanged(index, switch1.checked);
                    }
                }

                Label {
                    id: label2
                    text: qsTr("Switch")
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                }

            }

            Column {
                id: column1
                width: Window.width/3
                Layout.minimumWidth: 200
                Layout.fillHeight: true

                Label {
                    id: label
                    anchors.centerIn: parent
                    text: qsTr("Label")
                }
            }

            Column {
                id: column2
                width: Window.width/3
                Layout.minimumWidth: 200
                Layout.fillHeight: true

                Label {
                    id: label1
                    anchors.centerIn: parent
                    text: qsTr("Label")
                }
            }


        }


        //        Row {
        //            id: row2
        //            width: 200
        //            height: 400
        //            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        //            Layout.fillHeight: true
        //            Layout.fillWidth: false

        //            Image {
        //                id: motorImage
        //                anchors.horizontalCenter: parent.horizontalCenter
        //                sourceSize.height: 300
        //                sourceSize.width: 300
        //                antialiasing: false
        //                fillMode: Image.PreserveAspectFit
        //                source: "img/motor_run.svg"
        //                RotationAnimator{
        //                    id: animator
        //                    target: motorImage
        //                    from: 0;
        //                    to: 360;
        //                    duration: 2000
        //                    running: true
        //                    loops: Animation.Infinite
        //                }

        //            }
        //        }

        //        Row{
        //            id: actionButtonsRow
        //            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        //            spacing: 15

        //            Button {
        //                id: pumpButton
        //                width: 100
        //                height: 80
        //                text: qsTr("Pump")
        //                highlighted: true
        //                onClicked: {
        //                    devicePage.actionButtonClicked(listView.currentIndex, true);
        //                }
        //            }
        //            Button {
        //                id: stopButton
        //                width: 100
        //                height: 80
        //                text: qsTr("Stop")
        //                onClicked: {
        //                    devicePage.actionButtonClicked(listView.currentIndex, false);
        //                }
        //            }

        //        }
        //    }
    }


    function setTitleLabelVisiable(isVisiable){
        devicePage.titleLabel.visible = isVisiable;
    }

    function updateDeviceInfo(){

    }
}





//import QtQuick 2.7

//DevicePageForm {

//    button1.onClicked: {
//        console.log("Button Pressed. Entered text: " + textField1.text);
//    }
//}
