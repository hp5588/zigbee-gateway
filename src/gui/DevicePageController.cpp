#include <iomanip>
#include <sstream>
#include "DevicePageController.h"

DevicePageController::DevicePageController()
{}


void DevicePageController::init(){
    /*logger*/
    logger = spdlog::stdout_color_mt("DevicePageController");


    /*init UI*/
    engine->rootContext()->setContextProperty("devicePageViewController", this);
    engine->rootContext()->setContextProperty("listModel", QVariant::fromValue(devices));
    engine->load(QUrl(QLatin1String("qrc:/qml/main.qml")));

    this->rootObject = engine->rootObjects().back();
    iconMap = rootObject->findChild<QObject*>("iconMap",Qt::FindChildrenRecursively);
    QObject* devicePageViewObject = rootObject->findChild<QObject*>("devicePage",Qt::FindChildrenRecursively);

    QObject::connect(devicePageViewObject,SIGNAL(motorStateChanged(int,bool)), this, SLOT(motorStateChanged(int,bool)));
    QObject::connect(iconMap,SIGNAL(updateFinished()), this, SLOT(onIconMapUpdateFinished()));
    refreshIconMap();


//    QList<QObject*> deviceIconObjects = rootObject->findChildren<QObject*>("deviceIcon",Qt::FindChildrenRecursively);
//    for (int i = 0; i < deviceIconObjects.count(); ++i) {
//        QObject::connect(deviceIconObjects.at(i),SIGNAL(iconClicked(int)), this, SLOT(iconClick(int)));
//    }



    /*register callback*/
    app->registerDeviceJoinCB(std::bind(&DevicePageController::deviceJoined,this,std::placeholders::_1));



}



void DevicePageController::updatePageView() {

}

DevicePageController::DevicePageController(QList<QObject *> devices) {
    this->devices = devices;
}



void DevicePageController::setEngine(QQmlApplicationEngine *engine) {
    DevicePageController::engine = engine;
}

Coordinator *DevicePageController::getCoordinator() const {
    return coordinator;
}

void DevicePageController::setCoordinator(Coordinator *coordinator) {
    DevicePageController::coordinator = coordinator;
}

CoordinatorApp *DevicePageController::getApp() const {
    return app;
}

void DevicePageController::setApp(CoordinatorApp *app) {
    DevicePageController::app = app;
}

const QList<QObject *> &DevicePageController::getDevices() const {
    return devices;
}

void DevicePageController::setDevices(const QList<QObject *> &devices) {
    DevicePageController::devices = devices;
}

void DevicePageController::send(int index, uint8_t *data, uint8_t length) {
    uint16_t addr = ((DeviceObject*)(devices.at(index)))->shortAddr();
    std::stringstream ss;
    ss <<  std::hex;
    for (int i = 0; i < 2; ++i) {
        ss << std::setfill('0') << std::setw(2) << (int) (((uint8_t *)&addr)[i]);
    }
    logger->debug("send to addr " + ss.str());
    logger->debug("sizeof data " + std::to_string(sizeof(data)));
    this->coordinator->sendData(addr,data,length);
}






