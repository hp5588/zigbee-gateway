#ifndef DEVICEPAGECONTROLLER_H
#define DEVICEPAGECONTROLLER_H

class CoordinatorApp;
#include <src/devices/DeviceModel.h>

#include <QObject>
#include <DeviceObject.h>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <src/devices/Coordinator.h>
#include <src/CoordinatorApp.h>


//#include "DeviceObject.h"

class DevicePageController : public QObject
{
    Q_OBJECT
public:
    DevicePageController();
    DevicePageController(QList<QObject*> devices);

//    DevicePageController(Coordinator &coordinator);

    void init();
    void updatePageView();
private:
    std::shared_ptr<spdlog::logger> logger;

    QQmlApplicationEngine *engine;
    QList<QObject*> devices;

private:


    QObject *rootObject;
    QObject* iconMap;


    Coordinator * coordinator;


private:
    CoordinatorApp *app;


public:

    void setEngine(QQmlApplicationEngine *engine);

    const QList<QObject *> &getDevices() const;
    void setDevices(const QList<QObject *> &devices);

    void setCoordinator(Coordinator *coordinator);
    Coordinator *getCoordinator() const;


    CoordinatorApp *getApp() const;
    void setApp(CoordinatorApp *app);


    /*callback from app*/
    void deviceJoined(DeviceModel &deviceModel){
        /*add to QList and call*/
        logger->debug("deviceJoined");
        DeviceObject *device = new DeviceObject("New1",DeviceObject::Motor,200,200);
        device->setShortAddr(deviceModel.getShortAddr());
        devices.push_back(device);
        refreshIconMap();
    };
    void deviceConneced(){

    };


    void actionComplete(){

    };

    void dataHandler(uint8_t *data, uint8_t length){

    }


    /*UI callbacl*/
public slots:
    void motorStateChanged(int index, bool active){
        uint8_t data[2] = {0x11,0x22};
/*        if (active){
            data[0] = 0x04;
        } else{
            data[0] = 0x05;
        }*/
        send(index, data, 2);
        qDebug() << "motorStateChanged index:" << index << " active:" << active ;
    }

    void iconClick(int index){
        engine->rootContext()->setContextProperty("currentDevice", QVariant::fromValue(devices.at(index)));
        engine->rootContext()->setContextProperty("currentIndex", QVariant::fromValue(index));

        qDebug() << "iconClick index:" << index ;
    }

    void onIconMapUpdateFinished(){

        connectIcons();

    }

private:
    void refreshIconMap(){
        engine->rootContext()->setContextProperty("listModel", QVariant::fromValue(devices));
        QMetaObject::invokeMethod(iconMap,"update");
    }

    void connectIcons(){
        /*link icons to update method*/
        QList<QObject*> deviceIconObjects = rootObject->findChildren<QObject*>("deviceIcon",Qt::FindChildrenRecursively);
        logger->debug("deviceIconObjects length:" +  std::to_string(deviceIconObjects.length()));
        for (int i = 0; i < deviceIconObjects.count(); ++i) {
            QObject::connect(deviceIconObjects.at(i),SIGNAL(iconClicked(int)), this, SLOT(iconClick(int)));
        }
    }
    void send(int index, uint8_t *data, uint8_t length);

};

#endif // DEVICEPAGECONTROLLER_H
