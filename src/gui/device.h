#ifndef DEVICE_H
#define DEVICE_H
#include <QtCore/QMetaType>
#include <QObject>
//enum DeviceType{
//    Motor,
//    WaterTank
//};
//Q_DECLARE_METATYPE(DeviceType)


class Device: public QObject
{


    Q_OBJECT

     Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY deviceNameChanged)
//     Q_PROPERTY(DeviceType deviceType READ deviceType WRITE setDeviceType NOTIFY deviceTypeChanged)


   signals:
    void deviceNameChanged(QString deviceName);
//    void deviceTypeChanged(DeviceType deviceType);

public:
    Device();
    Device(const QString deviceName);
//    Device(const QString deviceName, DeviceType deviceType);


    void setDeviceName(QString deviceName){
        this->deviceName = deviceName;
        emit deviceNameChanged(deviceName);
    }
//    void setDeviceType(DeviceType deviceType){
//        this->deviceType = deviceType;
//        emit deviceTypeChanged(deviceType);
//    }


private:
    QString deviceName;
//    DeviceType deviceType;


};

#endif // DEVICE_H
