#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <iostream>
#include <QDebug>
#include <DeviceObject.h>
#include <QQmlContext>
#include <DevicePageController.h>

//class SignalHandler: public QObject
//{
//   Q_OBJECT
//   public slots:
//       void cppSlot(const QString &msg) {
//           std::cout << "Called the C++ slot with message:" << msg;
//           qDebug() << "Called the C++ slot with message:" << msg;
//       }
//};

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);


    QList<QObject*> devices;
    devices.append(new DeviceObject("Test Name", DeviceObject::DeviceType::Motor, 100, 100));
    devices.append(new DeviceObject("Test Name2", DeviceObject::DeviceType::WaterTank,50 ,50));
//    devices.at(0)->setProperty("x",QVariant::fromValue(300));
//    devices.at(0)->setProperty("y",QVariant::fromValue(50));
//    devices.at(1)->setProperty("x",QVariant::fromValue(50));
//    devices.at(1)->setProperty("y",QVariant::fromValue(60));


    DevicePageController devicePageViewController(devices);
//    DevicePageController devicePageViewController;

    QQmlApplicationEngine engine;
    devicePageViewController.setEngine(&engine);
    devicePageViewController.init();
//    engine.rootContext()->setContextProperty("myObject", &myObject);
//    engine.rootContext()->setContextProperty("devicePageViewController", &devicePageViewController);
//    engine.rootContext()->setContextProperty("listModel", QVariant::fromValue(devices));
//    engine.load(QUrl(QLatin1String("qrc:/qml/main.qml")));
/*
    QObject *rootObject = engine.rootObjects().back();

    QObject* devicePageViewObject = rootObject->findChild<QObject*>("devicePage",Qt::FindChildrenRecursively);
    QObject::connect(devicePageViewObject,SIGNAL(motorStateChanged(int,bool)), &devicePageViewController, SLOT(motorStateChanged(int,bool)));

    QList<QObject*> deviceIconObjects = rootObject->findChildren<QObject*>("deviceIcon",Qt::FindChildrenRecursively);

    for (int i = 0; i < deviceIconObjects.count(); ++i) {
        QObject::connect(deviceIconObjects.at(i),SIGNAL(iconClicked(int)), &devicePageViewController, SLOT(iconClick(int)));
    }
*/

//    QObject::connect(devicePageViewObject,SIGNAL(itemTouched(int)), &myObject, SLOT(cppSlot(int)));
//    QObject::connect(devicePageViewObject,SIGNAL(actionButtonClicked(int, bool)), &devicePageViewController, SLOT(actionButtonPressed(int,bool)));


    return app.exec();
}

