//
// Created by brian on 4/18/17.
//

#ifndef ZIGBEE_COORDINATOR_APP_H
#define ZIGBEE_COORDINATOR_APP_H
class DevicePageController;
#include <src/devices/Coordinator.h>
#include <src/gui/DevicePageController.h>
#include <thread>
#include <vector>
#include <install/include/spdlog/spdlog.h>
#include <QtCore/QObject>
#include "ZigbeeZnp.h"
#include "AppBase.h"
#include <src/devices/DeviceModel.h>



class CoordinatorApp : public AppBase<Coordinator>{
public:
    CoordinatorApp();
    void run();

    void registerDeviceJoinCB(std::function<void(DeviceModel&)> callback);

private:

    bool init() override;
    void initGui();


    QList<QObject*> devices;
//    DevicePageController devicePageViewController;
    DevicePageController *devicePageViewController;

    CoordinatorApp( const CoordinatorApp& other ) = delete;
    CoordinatorApp& operator=( const CoordinatorApp& ) = delete;

    /*callbacks*/
    std::function<void(DeviceModel&)> deviceJoinCB;



protected:


    void sysAreqHandler(Response *response);
    void sapiAreqHandler(Response *response);
    void zdoAreqHandler(Response *response) override;
    void appConfigAreqHandler(Response *response) override;

    void afAreqHandler(Response *response) override;

public:
    const QList<QObject *> &getDevices() const;

};

#endif //ZIGBEE_COORDINATOR_APP_H
