//
// Created by brian on 4/18/17.
//

#include <src/define/MT.h>
#include <sstream>
#include <iomanip>
#include <src/devices/DeviceModel.h>
#include <QtCore/QCoreApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QtGui/QGuiApplication>
#include <src/gui/DeviceObject.h>
#include "CoordinatorApp.h"
#include <variable.h>

bool CoordinatorApp::init() {


    initGui();

    ZigbeeZnp *znp = new ZigbeeZnp(DeviceBase::Coordinator, APP_PANID);
    device = (Coordinator *) znp->open();
    spdlog::stdout_color_mt("Response");

    devicePageViewController->setCoordinator(device);

    return false;
}


void CoordinatorApp::run() {
    init();
//    device->scan();
//    syncRsps.clear();
//    device->getDeviceInfo();
//    while (syncRsps.size()<8)
//        usleep(100000);
//
//    for (int i = 0; i < 8; ++i) {
//        lockSyncRspsMutex();
//        Response response = syncRsps.back();
//        std::stringstream ss;
//        ss <<  std::hex  << (int)response.getData()[0] << "-> " ;
//        for (int j = 0; j < 8; ++j) {
//            ss << std::setfill('0') << std::setw(2) << (int) *(response.getData()+1+j);
//        }
//        logger->debug(ss.str());
//        syncRsps.pop_back();
//        releaseSyncRspsMutex();
//    }
    /*register callback*/
    std::thread rThread (&CoordinatorApp::rxThread, this);

    device->appSetChannel(25, true);
    device->appSetChannel(0, false);

    syncRsps.clear();
    device->getDeviceInfo();
    while (syncRsps.size()<1);
    syncRsps.back().debugPrint();

    device->appStartCommission(NetworkFormation);
//    device->permitJoin();

//    while (!ready);



//    device->permitJoin();
//    device->allowBinding();

    while(true){
        /*test sending data once per second*/
        /*TODO for debugging, remove later*/
//        device->broadcast();
//        syncRsps.clear();
//        Response *response = readSrsp();
/*        if (networkDevicies.size()){
            static uint8_t count = 20;
            uint8_t data[] = {0xF1, count++};

//            device->sendData(networkDevicies.back().getShortAddr(),data,sizeof(data));
            device->sendData(networkDevicies.back().getShortAddr(),data,2);
        }*/


        std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    }

}


void CoordinatorApp::zdoAreqHandler(Response *response) {
    switch (response->getCmdId()){
        case MT_ZDO_STATE_CHANGE_IND:{
            logger->debug("MT_ZDO_STATE_CHANGE_IND");
            logger->debug("status: " + std::to_string(response->getData()[0]));
            if (response->getData()[0]==0x09) {
                logger->info("network ready");
                ready = true;
            }
            break;
        }
        case MT_ZDO_TC_DEVICE_IND:{
            /*new device join*/
            logger->debug("MT_ZDO_TC_DEVICE_IND");
            logger->info("new device join");
            DeviceModel device = DeviceModel(*response);
            networkDevicies.push_back(device);

            if (deviceJoinCB){
                deviceJoinCB(device);
            }
            response->debugPrint();



        }
        default:break;
    }
}

void CoordinatorApp::appConfigAreqHandler(Response *response) {


    switch (response->getCmdId()){
        case MT_APP_CNF_BDB_COMMISSIONING_NOTIFICATION:{
            uint8_t remaining = response->getData()[2];
            BdbCommissionMode mode = (BdbCommissionMode) response->getData()[1];
            BdbCommissionStatus status = (BdbCommissionStatus)response->getData()[0];

            logger->debug("MT_APP_CNF_BDB_COMMISSIONING_NOTIFICATION -> status:" + std::to_string(status)
                          +" mode:" + std::to_string(mode)
                          +" remain:" + std::to_string(remaining)
            );
            switch (mode){
                case BDB_COMMISSIONING_FORMATION:{
                    switch (status){
                        case BDB_COMMISSIONING_SUCCESS:{
                            /*trigger steering to allow joining*/
                            logger->info("network formation succeed");
                            device->appStartCommission(NetworkSteering);
                            break;
                        }
                        case BDB_COMMISSIONING_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_NO_NETWORK:break;
                        case BDB_COMMISSIONING_TL_TARGET_FAILURE:break;
                        case BDB_COMMISSIONING_TL_NOT_AA_CAPABLE:break;
                        case BDB_COMMISSIONING_TL_NO_SCAN_RESPONSE:break;
                        case BDB_COMMISSIONING_TL_NOT_PERMITTED:break;
                        case BDB_COMMISSIONING_TCLK_EX_FAILURE:break;
                        case BDB_COMMISSIONING_FORMATION_FAILURE:break;
                        case BDB_COMMISSIONING_FB_TARGET_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_INITITATOR_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_NO_IDENTIFY_QUERY_RESPONSE:break;
                        case BDB_COMMISSIONING_FB_BINDING_TABLE_FULL:break;
                        case BDB_COMMISSIONING_NETWORK_RESTORED:break;
                        case BDB_COMMISSIONING_FAILURE:break;
                    }
                    break;
                }
                case BDB_COMMISSIONING_NWK_STEERING: {
                    switch ((BdbCommissionStatus)response->getData()[0]){
                        case BDB_COMMISSIONING_SUCCESS:{
                            logger->info("network steering succeed");
                            break;
                        }
                        case BDB_COMMISSIONING_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_NO_NETWORK:break;
                        case BDB_COMMISSIONING_TL_TARGET_FAILURE:break;
                        case BDB_COMMISSIONING_TL_NOT_AA_CAPABLE:break;
                        case BDB_COMMISSIONING_TL_NO_SCAN_RESPONSE:break;
                        case BDB_COMMISSIONING_TL_NOT_PERMITTED:break;
                        case BDB_COMMISSIONING_TCLK_EX_FAILURE:break;
                        case BDB_COMMISSIONING_FORMATION_FAILURE:break;
                        case BDB_COMMISSIONING_FB_TARGET_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_INITITATOR_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_NO_IDENTIFY_QUERY_RESPONSE:break;
                        case BDB_COMMISSIONING_FB_BINDING_TABLE_FULL:break;
                        case BDB_COMMISSIONING_NETWORK_RESTORED:break;
                        case BDB_COMMISSIONING_FAILURE:break;
                    }
                    break;
                }
                case BDB_COMMISSIONING_INITIALIZATION:{
                    switch ((BdbCommissionStatus)response->getData()[0]){
                        case BDB_COMMISSIONING_SUCCESS:break;
                        case BDB_COMMISSIONING_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_NO_NETWORK:break;
                        case BDB_COMMISSIONING_TL_TARGET_FAILURE:break;
                        case BDB_COMMISSIONING_TL_NOT_AA_CAPABLE:break;
                        case BDB_COMMISSIONING_TL_NO_SCAN_RESPONSE:break;
                        case BDB_COMMISSIONING_TL_NOT_PERMITTED:break;
                        case BDB_COMMISSIONING_TCLK_EX_FAILURE:break;
                        case BDB_COMMISSIONING_FORMATION_FAILURE:break;
                        case BDB_COMMISSIONING_FB_TARGET_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_INITITATOR_IN_PROGRESS:break;
                        case BDB_COMMISSIONING_FB_NO_IDENTIFY_QUERY_RESPONSE:break;
                        case BDB_COMMISSIONING_FB_BINDING_TABLE_FULL:break;
                        case BDB_COMMISSIONING_NETWORK_RESTORED:break;
                        case BDB_COMMISSIONING_FAILURE:break;
                    }
                    break;
                }
                case BDB_COMMISSIONING_FINDING_BINDING:{

                    break;
                }
            }
        }
    }
}

void CoordinatorApp::sysAreqHandler(Response *response) {

}

void CoordinatorApp::afAreqHandler(Response *response) {
    switch (response->getCmdId()) {
        case MT_AF_INCOMING_MSG:{
            logger->debug("MT_AF_INCOMING_MSG -> length:" + std::to_string(response->getData()[18]));
            break;
        }
        default:break;
    }
}

void CoordinatorApp::sapiAreqHandler(Response *response) {
    std::string message;
    uint8_t status = 0;
    uint8_t *data;
    std::stringstream dataStringStream;
    switch (response->getCmdId()){
        case MT_SAPI_ALLOW_BIND_CNF:{
            logger->debug("MT_SAPI_ALLOW_BIND_CNF");
            break;
        }
        case MT_SAPI_SEND_DATA_CNF:{
            logger->debug("MT_SAPI_SEND_DATA_CNF");
            status = response->getData()[1];
            logger->debug("status: "+ std::to_string(status));
            break;
        }
        case MT_SAPI_RCV_DATA_IND:{
            logger->debug("MT_SAPI_RCV_DATA_IND");
            break;
        }
        case MT_SAPI_FIND_DEV_CNF:{
            logger->debug("MT_SAPI_FIND_DEV_CNF");
            data = response->getData() + 2;
            for (int i = 0; i < 8; ++i) {
                dataStringStream << std::hex << data++;
            }
            logger->debug("data string: ", dataStringStream.str());
            break;
        }


        default:{
            logger->debug("unhandled packet-> " + response->getCmdId());
            break;
        }
    }

}

CoordinatorApp::CoordinatorApp() {
    logger = spdlog::stdout_color_mt("CoordinatorApp");
    device = new Coordinator(DeviceBase::Coordinator, APP_PANID, COORDINATOR_DEVICE_ID);
}

void CoordinatorApp::initGui() {
    std::thread guiThread([&](){
//        int argc = 1;
//        char *argv[] = {(char *) "./coordinator"};


        QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
        QGuiApplication app(arc, arv);


//        QList<QObject*> devices;
        devices.append(new DeviceObject("Test Name", DeviceObject::DeviceType::Motor, 100, 100));
        devices.append(new DeviceObject("Test Name2", DeviceObject::DeviceType::WaterTank,50 ,50));

        QQmlApplicationEngine engine;
//        devicePageViewController = new DevicePageController(devices);
//        devicePageViewController.setDevices(devices);
//        DevicePageController devicePageViewController(devices);
        devicePageViewController = new DevicePageController();
//        DevicePageController devicePageViewController(*device);
        devicePageViewController->setEngine(&engine);
//        devicePageViewController.setCoordinator(*device);
        devicePageViewController->setApp(this);
        devicePageViewController->setDevices(devices);
        devicePageViewController->init();

        return app.exec();
    });
    guiThread.detach();

}

const QList<QObject *> &CoordinatorApp::getDevices() const {
    return devices;
}

void CoordinatorApp::registerDeviceJoinCB(std::function<void(DeviceModel&)> callback) {
    deviceJoinCB = callback;
}


