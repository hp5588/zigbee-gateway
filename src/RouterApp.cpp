//
// Created by brian on 5/11/17.
//

#include <src/define/MT.h>
#include <ios>
#include <sstream>
#include <iomanip>
#include "RouterApp.h"
#include "ZigbeeZnp.h"

//
// Created by brian on 4/18/17.
//


void RouterApp::run() {
    init();

    /*register callback*/
    std::thread rThread (&RouterApp::rxThread, this);

    syncRsps.clear();
    /*wait until bind to a device*/
//    device->getDeviceInfo();
//    while (syncRsps.size()<8);
//    for (int i = 0; i < 8; ++i) {
//        lockSyncRspsMutex();
//        Response response = syncRsps.back();
//        std::stringstream ss;
//        ss <<  std::hex  << (int)response.getData()[0] << "-> " ;
//        for (int j = 0; j < 8; ++j) {
//            ss << std::setfill('0') << std::setw(2) << (int) *(response.getData()+1+j);
//        }
//        logger->debug(ss.str());
//        syncRsps.pop_back();
//        releaseSyncRspsMutex();
//    }

    device->appSetChannel(25, true);
    device->appSetChannel(0, false);

    syncRsps.clear();
    device->getDeviceInfo();
    while (!syncRsps.size());
    syncRsps.back().debugPrint();

    device->appStartCommission(NetworkSteering);




//    while (!ready);




    /*bind device*/
//    device->bindDevices();


    while (true){
        /*wait for message send through AREQ*/
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    }
}

bool RouterApp::init() {
    ZigbeeZnp *znp = new ZigbeeZnp(DeviceBase::Router, APP_PANID);
    device = (Router *) znp->open();
    spdlog::stdout_color_mt("Response");

    return true;
}

void RouterApp::zdoAreqHandler(Response *response) {
    switch (response->getCmdId()){
        case MT_ZDO_STATE_CHANGE_IND:{
            logger->debug("MT_ZDO_STATE_CHANGE_IND");
            logger->debug("status: " + std::to_string(response->getData()[0]));
            if (response->getData()[0]==0x07){
                logger->info("network ready");
                ready = true;
            }
            break;
        }
        case MT_ZDO_TC_DEVICE_IND:{
            response->debugPrint();
            logger->debug("MT_ZDO_TC_DEVICE_IND");

            break;
        }

        default:break;
    }
}

void RouterApp::sysAreqHandler(Response *response) {

}

void RouterApp::afAreqHandler(Response *response) {
    switch (response->getCmdId()) {
        case MT_AF_INCOMING_MSG: {
            logger->debug("MT_AF_INCOMING_MSG -> length:" + std::to_string(response->getData()[18]));
            break;
        }
    }

}

void RouterApp::appConfigAreqHandler(Response *response) {

    switch (response->getCmdId()){
        case MT_APP_CNF_BDB_COMMISSIONING_NOTIFICATION:{
            uint8_t remaining = response->getData()[2];
            BdbCommissionMode mode = (BdbCommissionMode) response->getData()[1];
            BdbCommissionStatus status = (BdbCommissionStatus)response->getData()[0];

            logger->debug("MT_APP_CNF_BDB_COMMISSIONING_NOTIFICATION -> status:" + std::to_string(response->getData()[0])
                          +" mode:" + std::to_string(response->getData()[1])
                          +" remain:" + std::to_string(response->getData()[2])
            );

            switch (mode){
                case BDB_COMMISSIONING_INITIALIZATION:break;
                case BDB_COMMISSIONING_NWK_STEERING:{
                    switch (status){
                        case BDB_COMMISSIONING_SUCCESS:{
                            logger->debug("BDB_COMMISSIONING_SUCCESS");

                            break;
                        }
                        case BDB_COMMISSIONING_NO_NETWORK:{
                            logger->debug("BDB_COMMISSIONING_NO_NETWORK");

                            std::this_thread::sleep_for(std::chrono::milliseconds(3000));
                            logger->debug("try again");
                            device->appStartCommission(NetworkSteering);

                        }
                        default:{
                            logger->debug("BDB_COMMISSIONING_NWK_STEERING exception");
                            break;
                        }
                    }

                    break;
                }
                case BDB_COMMISSIONING_FORMATION:break;
                case BDB_COMMISSIONING_FINDING_BINDING:break;
            }

        }
    }
}

void RouterApp::sapiAreqHandler(Response *response) {
    switch (response->getCmdId()){
        case MT_SAPI_ALLOW_BIND_CNF:{
            logger->debug("MT_SAPI_ALLOW_BIND_CNF");
            break;
        }
        case MT_SAPI_BIND_CNF:{
            logger->debug("MT_SAPI_BIND_CNF");
            logger->debug("status: "+ std::to_string(response->getData()[2]));

        }
        case MT_SAPI_SEND_DATA_CNF:{
            logger->debug("MT_SAPI_SEND_DATA_CNF");
            logger->debug("status: "+ std::to_string(response->getData()[1]));
            break;
        }
        case MT_SAPI_RCV_DATA_IND:{
            logger->debug("MT_SAPI_RCV_DATA_IND");

            uint16_t src = *((uint16_t*)(response->getData()));
            uint16_t commandId = *((uint16_t*)(response->getData() + 2));
            uint16_t length = *((uint16_t*)(response->getData() + 4));
            std::stringstream ss;
            ss << std::hex ;
            for (int i = 0; i < length; ++i)
                ss << (int)*(response->getData() + 6+ i);
            logger->debug("src-> " + src);
            logger->debug("commandId-> " +commandId);
            logger->debug("data: " + ss.str());
            break;
        }
        case MT_SAPI_FIND_DEV_CNF:{
            logger->debug("MT_SAPI_FIND_DEV_CNF");
            break;
        }




        default:{
            logger->debug("unhandled packet");
            break;
        }
    }
}

RouterApp::RouterApp() {
    logger = spdlog::stdout_color_mt("RouterApp");
    device = new Router(DeviceBase::Router, APP_PANID, ROUTER_DEVICE_ID);
}
